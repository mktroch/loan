package com.example.loan.service

import com.example.loan.domain.loan.configuration.Amount
import com.example.loan.domain.loan.configuration.LoanConfiguration
import com.example.loan.domain.loan.configuration.Term
import org.javamoney.moneta.Money
import spock.lang.Specification

import java.time.Duration
import java.time.Instant

class LoanApplicationValidationServiceTest extends Specification {


    def "Loan application should be validated against business rules"() {
        LoanConfiguration configuration = new LoanConfiguration()
        Amount amount = new Amount()
        amount.setCurrency("USD")
        amount.setMax(100)
        amount.setMin(10)
        Term term = new Term()
        term.setMaxDays(30)
        term.setMinDays(7)
        configuration.setAmount(amount)
        configuration.setTerm(term)
        configuration.setExtensionDays(14)
        configuration.setInterestRate("10%")
        expect:
        new LoanApplicationValidationService(configuration).validate(a, b, c, UUID.randomUUID().toString()) == d

        where:
        a                     | b                                     | c                   || d
        Money.of(10, "USD")   | Instant.parse("2019-01-07T00:00:00Z") | Duration.ofDays(8)  || true
        Money.of(50, "USD")   | Instant.parse("2019-01-07T06:00:00Z") | Duration.ofDays(30) || true
        Money.of(9.99, "USD") | Instant.parse("2019-01-07T14:00:00Z") | Duration.ofDays(10) || false
        Money.of(100, "USD")  | Instant.parse("2019-01-07T05:59:59Z") | Duration.ofDays(10) || false
        Money.of(200, "USD")  | Instant.parse("2019-01-07T10:59:00Z") | Duration.ofDays(10) || false
    }
}
