package com.example.loan.service

import org.javamoney.moneta.Money
import spock.lang.Specification

import static javax.money.Monetary.getCurrency

class LoanCostCalculatorServiceTest extends Specification {
    def "GetLoanCost should return base loan amount with interest"() {
        expect:
        LoanCostCalculatorService.fromInterestRate(a).getLoanCost(b).isEqualTo(c)

        where:
        a      | b                                 || c
        "10%"  | Money.of(20, getCurrency("EUR"))  || Money.of(22, getCurrency("EUR"))
        "100%" | Money.of(1, getCurrency("EUR"))   || Money.of(2, getCurrency("EUR"))
        "10%"  | Money.of(1.5, getCurrency("EUR")) || Money.of(1.65, getCurrency("EUR"))
    }
}
