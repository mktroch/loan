package com.example.loan

import com.example.loan.controller.LoanController
import com.example.loan.domain.LoanDTO
import com.example.loan.domain.loan.LoanStatus
import com.example.loan.request.NewLoanApplicationRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import java.time.Instant
import java.util.function.Supplier

import static com.example.loan.domain.loan.LoanStatus.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@SpringBootTest
class LoanTest extends Specification {
    @Autowired
    MockMvc mockMvc
    @Autowired
    LoanController controller
    @Autowired
    Supplier<Instant> timeSupplier
    @Autowired
    ObjectMapper objectMapper
    @Autowired
    WebApplicationContext webApplicationContext

    void setup() {
        //    controller.setTimeSupplier(timeSupplier)
    }

    void cleanup() {
    }

    def "Loan request within amount, period and allowed daytime-amount rules is accepted"() {
        NewLoanApplicationRequest request = new NewLoanApplicationRequest();
        given: 'amount within allowed limits'
        request.setAmount(10.00)
        request.setCurrencyCode("USD")
        and: 'loan period within limits'
        request.setDuration(2)
        and: 'request not filled between 00:00 and 06:00'
        controller.setTimeSupplier({
            return Instant.parse("2018-11-20T17:24:18Z")
        })
        when: 'loan is requested'
        ResultActions action = mockMvc.perform(MockMvcRequestBuilders.post("/apply")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request)))

        then: 'response status is OK'
        action.andExpect(status().isOk())
        and: 'loan status indicates that loan application is accepted or waits for processing'
        String s = action.andReturn().getResponse().getContentAsString()
        LoanDTO applicationResponse = objectMapper.readValue(s, LoanDTO.class)
        LoanStatus status = applicationResponse.getStatus()
        status == ACCEPTED || status == IN_PROCESSING || status == NEW
    }
}
