package com.example.loan.service;

import com.example.loan.domain.loan.configuration.LoanConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.time.*;

@Service
public class LoanApplicationValidationService {

    private final LoanConfiguration configuration;
    private final MonetaryAmount maxAmount;

    @Autowired
    public LoanApplicationValidationService(LoanConfiguration configuration) {
        this.configuration = configuration;
        this.maxAmount = Monetary.getDefaultAmountFactory()
                .setCurrency(configuration.getAmount().getCurrency())
                .setNumber(configuration.getAmount().getMax())
                .create();
    }

    public Boolean validate(MonetaryAmount amount, Instant applicationDate, Duration term, String id) {
        return validateAmount(amount)
                && validateApplicationDateAndAmount(applicationDate, amount)
                && validatePeriod(term);
    }

    private boolean validateAmount(MonetaryAmount amount) {
        MonetaryAmount minAmount = Monetary.getDefaultAmountFactory()
                .setCurrency(configuration.getAmount().getCurrency())
                .setNumber(configuration.getAmount().getMin())
                .create();

        return amount.isGreaterThanOrEqualTo(minAmount) && amount.isLessThanOrEqualTo(maxAmount);
    }

    private boolean validateApplicationDateAndAmount(Instant applicationDate, MonetaryAmount amount) {
        Instant start = LocalDateTime.ofInstant(applicationDate, ZoneId.of("UTC")).withHour(0).withMinute(0).toInstant(ZoneOffset.UTC);
        Instant end = LocalDateTime.ofInstant(applicationDate, ZoneId.of("UTC")).withHour(6).withMinute(0).toInstant(ZoneOffset.UTC);
        return !(applicationDate.isAfter(start) && applicationDate.isBefore(end) && amount.isEqualTo(maxAmount));
    }

    private boolean validatePeriod(Duration term) {
        long days = term.toDays();
        return days >= configuration.getTerm().getMinDays() && days <= configuration.getTerm().getMaxDays();
    }
}
