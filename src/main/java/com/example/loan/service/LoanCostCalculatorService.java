package com.example.loan.service;

import com.example.loan.domain.loan.configuration.LoanConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.money.MonetaryAmount;
import java.text.NumberFormat;
import java.text.ParseException;

@Service
public class LoanCostCalculatorService {
    private final Number interestPercent;

    @Autowired
    public LoanCostCalculatorService(LoanConfiguration configuration) throws ParseException {
        this.interestPercent = NumberFormat.getPercentInstance().parse(configuration.getInterestRate());
    }

    public static LoanCostCalculatorService fromInterestRate(String interestRate) throws ParseException {
        //  LoanConfiguration configuration = LoanConfiguration.builder().interestRate(interestRate).build();

        LoanConfiguration configuration = new LoanConfiguration();
        configuration.setInterestRate(interestRate);
        return new LoanCostCalculatorService(configuration);
    }

    public MonetaryAmount getLoanCost(MonetaryAmount baseAmount) {
        return baseAmount.add(baseAmount.multiply(interestPercent));
    }
}
