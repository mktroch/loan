package com.example.loan.service;

import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Instant;
import java.util.function.Supplier;

@Service
public class TimeSupplier implements Supplier<Instant> {
    @Override
    public Instant get() {
        return Instant.now(Clock.systemUTC());
    }
}
