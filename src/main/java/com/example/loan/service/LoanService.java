package com.example.loan.service;

import com.example.loan.commands.ExtendLoanCommand;
import com.example.loan.commands.GrantLoanCommand;
import com.example.loan.commands.RejectLoanCommand;
import com.example.loan.domain.loan.configuration.LoanConfiguration;
import com.example.loan.events.AppliedForLoanEvent;
import com.example.loan.events.AppliedForLoanExtensionEvent;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class LoanService {
    private final LoanApplicationValidationService validationService;
    private final LoanCostCalculatorService costCalculatorService;
    private final CommandGateway commandGateway;
    private final LoanConfiguration configuration;

    @Autowired
    public LoanService(LoanApplicationValidationService validationService, LoanCostCalculatorService costCalculatorService, CommandGateway commandGateway, LoanConfiguration configuration) {
        this.validationService = validationService;
        this.costCalculatorService = costCalculatorService;
        this.commandGateway = commandGateway;
        this.configuration = configuration;
    }

    public CompletableFuture<UUID> process(AppliedForLoanEvent event) {
        if (validationService.validate(event.getAmount(), event.getApplied(), event.getDuration(), event.getUid())) {
            return commandGateway.send(new GrantLoanCommand(event.getId(),
                    event.getUid(),
                    event.getApplied().plus(event.getDuration()),
                    costCalculatorService.getLoanCost(event.getAmount())));
        } else {
            return commandGateway.send(new RejectLoanCommand(
                    event.getId(),
                    event.getUid(), Collections.singletonList("Validation criteria are not met")));
        }
    }

    @EventSourcingHandler
    public void extend(AppliedForLoanExtensionEvent event) {
        Instant extendedDueDate = event.getActualTerm().plus(Duration.ofDays(configuration.getExtensionDays()));
        commandGateway.send(new ExtendLoanCommand(
                event.getId(),
                extendedDueDate, event.getApplied()));
    }

}
