package com.example.loan.commands;

import lombok.Getter;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

@Getter
public class RejectLoanCommand {
    @TargetAggregateIdentifier
    private final String id;
    private final String uid;
    private final Iterable<String> rejectionReasons;

    public RejectLoanCommand(String id, String uid, Iterable<String> rejectionReasons) {
        this.id = id;
        this.uid = uid;
        this.rejectionReasons = rejectionReasons;
    }
}
