package com.example.loan.commands;

import com.example.loan.request.NewLoanApplicationRequest;
import lombok.Getter;
import org.axonframework.commandhandling.TargetAggregateIdentifier;
import org.javamoney.moneta.RoundedMoney;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Supplier;

@Getter
public class ApplyForLoanCommand {
    @TargetAggregateIdentifier
    private final String id;
    private final String customerId;
    private final Duration term;
    private final Instant applied;
    MonetaryAmount amount;
    private Supplier<Instant> timeSupplier;

    public ApplyForLoanCommand(String customerId, Integer termDays, Double amount, String currencyCode, Instant applicationTime) {
        term = Duration.ofDays(termDays);
        this.amount = RoundedMoney.from(Monetary.getDefaultAmountFactory()
                .setCurrency(currencyCode)
                .setNumber(new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP))
                .create());
        applied = applicationTime;
        this.id = UUID.randomUUID().toString();
        this.customerId = customerId;
    }

    public static ApplyForLoanCommand from(NewLoanApplicationRequest request, Supplier<Instant> timeSupplier) {
        return new ApplyForLoanCommand(request.getCustomerId(), request.getDuration(), request.getAmount(), request.getCurrencyCode(), timeSupplier.get());
    }
}
