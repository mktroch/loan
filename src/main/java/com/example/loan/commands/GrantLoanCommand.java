package com.example.loan.commands;

import lombok.Getter;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import javax.money.MonetaryAmount;
import java.time.Instant;

@Getter
public class GrantLoanCommand {
    @TargetAggregateIdentifier
    private final String id;
    private final String uid;
    private final Instant dueDate;
    private final MonetaryAmount totalCost;

    public GrantLoanCommand(String id, String uid, Instant dueDate, MonetaryAmount totalCost) {
        this.id = id;
        this.uid = uid;
        this.dueDate = dueDate;
        this.totalCost = totalCost;
    }
}
