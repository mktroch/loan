package com.example.loan.commands;

import lombok.Data;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.time.Instant;

@Data
public class ApplyForLoanExtensionCommand {
    @TargetAggregateIdentifier
    private final String id;
    private final Instant applied;
}
