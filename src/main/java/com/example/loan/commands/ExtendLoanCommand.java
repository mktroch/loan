package com.example.loan.commands;

import lombok.Data;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.time.Instant;

@Data
public class ExtendLoanCommand {
    @TargetAggregateIdentifier
    private final String id;
    private final Instant extendedDueDate;
    private final Instant applied;


}
