package com.example.loan;

import com.example.loan.domain.loan.Loan;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.commandhandling.gateway.DefaultCommandGateway;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.config.EventProcessingConfiguration;
import org.axonframework.eventhandling.tokenstore.TokenStore;
import org.axonframework.eventhandling.tokenstore.jpa.JpaTokenStore;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.axonframework.monitoring.MessageMonitor;
import org.axonframework.serialization.xml.XStreamSerializer;
import org.axonframework.spring.config.annotation.AnnotationCommandHandlerBeanPostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;


@Configuration
public class LoanApplicationConfiguration {


    @Autowired
    public LoanApplicationConfiguration() {
    }

    @Bean
    public EventProcessingConfiguration getEventProcessingConfiguration() {
        return new EventProcessingConfiguration();
    }

    @Bean
    public EventStorageEngine eventStorageEngine(EntityManagerProvider provider, TransactionManager transactionManager) throws SQLException {
        return new JpaEventStorageEngine(provider, transactionManager);

    }

    @Bean
    public TokenStore getTokenStore(EntityManagerProvider provider) {
        return new JpaTokenStore(provider, new XStreamSerializer());
    }

    @Bean
    public CommandGateway getCommandGateway(CommandBus commandBus) {
        return new DefaultCommandGateway(commandBus);
    }

    @Bean
    public EventStore getEventStore(EventStorageEngine engine) {
        return new EmbeddedEventStore(engine);
    }

    @Bean
    @Autowired
    public EventSourcingRepository<Loan> getEventSourcingRepository(EventStore eventStore) {
        return new EventSourcingRepository<>(Loan.class, eventStore);
    }


    @Bean
    public CommandBus commandBus(TransactionManager transactionManager) {
        return new SimpleCommandBus(transactionManager, message -> new MessageMonitor.MonitorCallback() {
            @Override
            public void reportSuccess() {

            }

            @Override
            public void reportFailure(Throwable cause) {

            }

            @Override
            public void reportIgnored() {

            }
        });

    }

    @Bean
    AnnotationCommandHandlerBeanPostProcessor
    annotationCommandHandlerBeanPostProcessor() {
        AnnotationCommandHandlerBeanPostProcessor handler =
                new AnnotationCommandHandlerBeanPostProcessor();
        return handler;
    }
}
