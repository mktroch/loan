package com.example.loan.request;

import lombok.Data;

@Data
public class NewLoanApplicationRequest {
    String customerId;
    Double amount;
    String currencyCode;
    Integer duration;
}
