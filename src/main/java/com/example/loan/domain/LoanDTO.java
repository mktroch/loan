package com.example.loan.domain;

import com.example.loan.domain.loan.Loan;
import com.example.loan.domain.loan.LoanStatus;
import lombok.Getter;
import lombok.Setter;
import org.javamoney.moneta.RoundedMoney;

import java.time.Instant;

@Getter
@Setter
public class LoanDTO {
    private String id;
    private LoanStatus status;
    private Double amount;
    private String currency;
    private Instant date;
    private Instant dueDate;

    public LoanDTO(Loan loan) {
        id = loan.getId();
        status = loan.getStatus();
        date = loan.getApplied();
        this.amount = RoundedMoney.from(loan.getAmount()).getNumber().doubleValue();
        this.currency = loan.getAmount().getCurrency().getCurrencyCode();
        this.dueDate = loan.getDueDate();
    }

    public LoanDTO() {
    }

    public static LoanDTO noSuchLoan(String id) {
        LoanDTO dto = new LoanDTO();
        dto.setId(id);
        return dto;
    }


}