package com.example.loan.domain.loan;

public enum LoanStatus {
    UNKNOWN,
    NEW,
    IN_PROCESSING,
    ACCEPTED,
    EXTENDED,
    REJECTED,
    NO_LOAN
}
