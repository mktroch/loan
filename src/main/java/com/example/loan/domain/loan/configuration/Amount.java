package com.example.loan.domain.loan.configuration;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Amount {
    private String currency;
    private Integer min;
    private Integer max;


}
