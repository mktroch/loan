package com.example.loan.domain.loan;

import com.example.loan.commands.*;
import com.example.loan.events.*;
import com.example.loan.repository.LoanRepository;
import lombok.Getter;
import lombok.Setter;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.factory.annotation.Autowired;

import javax.money.MonetaryAmount;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.Instant;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Entity
@Table(name = "LOAN")
@Aggregate
@Getter
@Setter
public class Loan {

    private final static long serialVersionUID = 1L;
    @Id
    @AggregateIdentifier
    protected String id;
    protected String uid;

    protected MonetaryAmount amount;
    protected Instant dueDate;
    protected Instant applied;
    protected LoanStatus status;

    @Autowired
    @Transient
    private LoanRepository repository;

    @CommandHandler
    public Loan(ApplyForLoanCommand command) {
        this();
        this.status = LoanStatus.NEW;
        apply(new AppliedForLoanEvent(
                command.getId(),
                command.getCustomerId(),
                command.getAmount(),
                command.getApplied(),
                command.getTerm()));
        this.status = LoanStatus.IN_PROCESSING;
    }

    @Autowired
    public Loan() {
        status = LoanStatus.UNKNOWN;

    }

    @CommandHandler
    public void onCommand(GrantLoanCommand command) {
        apply(new AcceptedLoanEvent(command.getId(), command.getTotalCost(), command.getDueDate()));
    }

    @CommandHandler
    public void onCommand(RejectLoanCommand command) {
        apply(new RejectedLoanEvent(command.getId(), command.getRejectionReasons()));
    }

    @CommandHandler
    public void onCommand(ApplyForLoanExtensionCommand command) {
        apply(new AppliedForLoanExtensionEvent(command.getId(), command.getApplied(), this.dueDate));
    }

    @CommandHandler
    public void onCommand(ExtendLoanCommand command) {
        apply(new ExtendedLoanEvent(command.getId(), command.getExtendedDueDate(), command.getApplied()));
    }

    @EventSourcingHandler
    public void onEvent(AppliedForLoanEvent event) {
        this.applied = event.getApplied();
        this.amount = event.getAmount();
        this.status = LoanStatus.NEW;
        this.uid = event.getUid();
        this.id = event.getId();
        this.dueDate = this.applied.plus(event.getDuration());
    }

    @EventSourcingHandler
    public void onEvent(AcceptedLoanEvent event) {
        this.dueDate = event.getDueDate();
        this.amount = event.getTotalAmount();
        this.status = LoanStatus.ACCEPTED;
    }

    @EventSourcingHandler
    public void onEvent(ExtendedLoanEvent event) {
        this.dueDate = event.getExtendedDueDate();
        this.status = LoanStatus.EXTENDED;
    }

    @EventSourcingHandler
    public void onEvent(RejectedLoanEvent event) {
        this.status = LoanStatus.REJECTED;
    }
}
