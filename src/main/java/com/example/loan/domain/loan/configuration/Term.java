package com.example.loan.domain.loan.configuration;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Term {
    private Integer minDays;
    private Integer maxDays;


}
