package com.example.loan.domain.loan.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "loan")
@Getter
@Setter
public class LoanConfiguration {
    @NestedConfigurationProperty
    private
    Term term;
    @NestedConfigurationProperty
    private
    Amount amount;
    private Integer extensionDays;
    private String interestRate;
}
