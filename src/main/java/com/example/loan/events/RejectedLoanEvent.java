package com.example.loan.events;

import lombok.Getter;
import org.axonframework.serialization.Revision;

@Getter
@Revision("1")
public class RejectedLoanEvent {
    private final String id;
    private final Iterable<String> reason;

    public RejectedLoanEvent(String id, Iterable<String> reason) {
        this.id = id;
        this.reason = reason;
    }
}
