package com.example.loan.events;

import lombok.Data;
import org.axonframework.serialization.Revision;

import java.time.Instant;

@Data
@Revision("1")
public class AppliedForLoanExtensionEvent {
    private final String id;
    private final Instant applied;
    private final Instant actualTerm;
}
