package com.example.loan.events;

import lombok.Getter;
import org.axonframework.serialization.Revision;

import javax.money.MonetaryAmount;
import java.time.Duration;
import java.time.Instant;

@Getter
@Revision("1")
public class AppliedForLoanEvent {

    private final String id;
    private final String uid;
    private final MonetaryAmount amount;
    private final Instant applied;
    private final Duration duration;

    public AppliedForLoanEvent(String id, String uid, MonetaryAmount amount, Instant applied, Duration duration) {
        this.id = id;
        this.uid = uid;
        this.amount = amount;
        this.applied = applied;
        this.duration = duration;
    }
}
