package com.example.loan.events;

import lombok.Getter;
import org.axonframework.serialization.Revision;

import javax.money.MonetaryAmount;
import java.time.Instant;

@Getter
@Revision("1")
public class AcceptedLoanEvent {
    private final String id;
    private final MonetaryAmount totalAmount;
    private final Instant dueDate;

    public AcceptedLoanEvent(String id, MonetaryAmount totalAmount, Instant dueDate) {
        this.id = id;
        this.totalAmount = totalAmount;
        this.dueDate = dueDate;
    }
}
