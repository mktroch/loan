package com.example.loan.events;

import lombok.Data;
import org.axonframework.serialization.Revision;

import java.time.Instant;

@Data
@Revision("1")
public class ExtendedLoanEvent {
    private final String id;
    private final Instant extendedDueDate;
    private final Instant applied;
}
