package com.example.loan.controller;

import com.example.loan.commands.ApplyForLoanCommand;
import com.example.loan.commands.ApplyForLoanExtensionCommand;
import com.example.loan.domain.LoanDTO;
import com.example.loan.query.LoanFilter;
import com.example.loan.query.LoanQuery;
import com.example.loan.query.LoanSummaryProvider;
import com.example.loan.request.NewLoanApplicationRequest;
import lombok.Getter;
import lombok.Setter;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

@RestController
public class LoanController {
    private final CommandGateway commandGateway;
    private final LoanSummaryProvider provider;

    @Setter
    @Getter
    private Supplier<Instant> timeSupplier;

    @Autowired
    public LoanController(CommandGateway commandGateway, LoanSummaryProvider provider, Supplier<Instant> timeSupplier) {
        this.commandGateway = commandGateway;
        this.provider = provider;

        this.timeSupplier = timeSupplier;
    }

    @PostMapping("/apply")
    public LoanDTO applyForLoan(@RequestBody NewLoanApplicationRequest newLoanApplicationRequest) throws ExecutionException, InterruptedException {
        String id = (String) commandGateway.send(ApplyForLoanCommand.from(newLoanApplicationRequest, timeSupplier)).get();
        LoanQuery query = new LoanQuery(new LoanFilter(id));
        return provider.query(query);
    }

    @GetMapping("/loan/{id}")
    public LoanDTO queryLoan(@PathVariable String id) {
        LoanQuery query = new LoanQuery(new LoanFilter(id));
        return provider.query(query);
    }

    @PostMapping("/loan/{id}/extend")
    public LoanDTO extendLoan(@PathVariable String id) {
        LoanQuery query = new LoanQuery(new LoanFilter(id));
        if (provider.loanExists(id)) {
            ApplyForLoanExtensionCommand command = new ApplyForLoanExtensionCommand(id, timeSupplier.get());
            commandGateway.send(command);
            return provider.query(query);
        }
        return LoanDTO.noSuchLoan(id);
    }
}
