package com.example.loan.query;

import lombok.Getter;

@Getter
public class LoanQuery {
    private final LoanFilter filter;

    public LoanQuery(LoanFilter filter) {
        this.filter = filter;
    }
}
