package com.example.loan.query;

import lombok.Getter;

@Getter
public class LoanFilter {
    private final String id;

    public LoanFilter(String id) {
        this.id = id;
    }
}
