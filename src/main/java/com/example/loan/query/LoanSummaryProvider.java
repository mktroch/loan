package com.example.loan.query;

import com.example.loan.domain.LoanDTO;
import com.example.loan.domain.loan.Loan;
import com.example.loan.repository.LoanRepository;
import org.axonframework.config.EventProcessingConfiguration;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LoanSummaryProvider {
    private final EventProcessingConfiguration eventProcessingConfiguration;
    private final LoanRepository repository;

    @Autowired
    public LoanSummaryProvider(EventProcessingConfiguration eventProcessingConfiguration, LoanRepository repository) {
        this.eventProcessingConfiguration = eventProcessingConfiguration;
        this.repository = repository;
    }

    @QueryHandler
    public boolean loanExists(String id) {
        return repository.existsById(id);
    }


    @QueryHandler
    public LoanDTO query(LoanQuery query) {
        String id = query.getFilter().getId();
        Optional<Loan> loan = repository.findById(id);
        if (loan.isPresent()) {
            return new LoanDTO(loan.get());
        }
        return LoanDTO.noSuchLoan(id);
    }

}
