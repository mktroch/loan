package com.example.loan.handler;

import com.example.loan.events.AppliedForLoanEvent;
import com.example.loan.service.LoanService;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
public class LoanHandler {

    private final CommandGateway commandGateway;
    private final LoanService loanService;

    public LoanHandler(CommandGateway commandGateway, LoanService loanService) {
        this.commandGateway = commandGateway;
        this.loanService = loanService;
    }

    @EventHandler
    public void handle(AppliedForLoanEvent event) {
        loanService.process(event);
    }
}
